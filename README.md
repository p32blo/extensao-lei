# [Firefox] Abrir Tópicos por ler em Separadores v1.2
>**_'Port'_ do extensão para chrome do Chronos para Firefox**

[addon]:https://bitbucket.org/p32blo/extensao-lei/downloads/extensaolei-v1.2.xpi
[latest]:https://bitbucket.org/p32blo/extensao-lei/downloads/extensaolei-latest.xpi
[custom]:http://support.mozilla.org/en-US/kb/customize-firefox-controls-buttons-and-toolbars


- - - - - - -
## Instalação:

1. [extensaolei.xpi][addon] (Permitir acesso se pedir)
1. ~~[extensaolei-latest.xpi][latest] - alternativamente usar a versao de desenvolvimento (só para testes!)~~
2. Se o download não for feito no Firefox basta **arrastar o ficheiro** recebido para uma janela do Firefox;
3. Clicar em "Instalar agora"

Para mudar o local do ícone: [customize-firefox-controls-buttons-and-toolbars][custom]

### Actualizações

Desde a versão 1.0 que estão disponíveis actualizações automáticas:

```text
Extras > Extensões > Click direito do Rato > Procurar Actualizações
```


- - - - -
## Opções:

```text
Extras > Extensões > Selecionar Extensão > Opções
```

- ** Endereço do Fórum ** - URL actual do fórum
- ** Máximo de Separadores ** - nº de separadores a abrir de cada vez
- ** Abrir **
	* `Unread` - Abrir tópicos activos por ler
	* `Unread Replies` - Abir tópicos participados por ler
	* `All Unread` - Abrir todos os tópicos por ler
- ** Focar no 1º separador **
	* `On` - Coloca o 1º separador não lido em foco (tira o foco ao separador actual)
	* `Off` - Abre todos os separadores em *background*
- ** Ordem de Abertura **
	* `Cronológica Inversa` - Abre 1º os separadores mais recentes
	* `Cronológica` - Abre 1º os separadores mais antigos
- ** Receber Notificações **
	* `On` - Activar notificações
	* `Off` - Desactivar notificações


- - - - - - -
## Changelog:

### - v1.2 -

#### ToDo


### - v1.1 -

#### Novas Funcionalidades
- Abrir separadores com *click* na notificação
- Preferência para mudar a frequência de verificação de novas mensagens

#### BugFixs
- URL podia nao abrir se a opção para abrir focado estivesse ativada
- Tipos de preferencia inteiros podiam ter valor fora do intervalo permitido


### - v1.0 -

#### Novas Funcionalidades
- Opção para limitar o número de separadores abertos de cada vez
- Actualizar área do Icon para a nova interface *Australis* `Firefox 29`
- Opção para abrir separadores por ordem inversa (Cronológica)
- Ativar addon no *Modo Privado*
- Notificações *toster-style*
- Actualizações automáticas

#### BugFixs
- Clicks sucessivos faziam com que os separdores se abrissem mais que uma vez
- O Icon ficava cortado
- Separadores não abriam se existisse um tópico chamado login