
self.port.on("Loaded", function () {
	// console.log("LOADED")
	self.port.on("data", openLinks);
	self.port.on("panel", panelWrite);
});

self.port.on("Unloaded", function () {
	// console.log("UNLOADED")
	panelWrite("");
	self.port.removeListener("data", openLinks);
	self.port.removeListener("panel", panelWrite);
});

self.port.on("unread", function (html) {
	// console.log("UNREAD IN");
	var links = getLinks(html);

	self.port.emit("notify", links);
	// console.log("UNREAD OUT");
});

self.port.on("clicked", function (list, prefs) {
	list = setPrefs(list, prefs);
	openTabs(list, prefs.focus);
});

// Writing to popup panel
function panelWrite (message) {
	//console.log("Got it ;)");
	document.getElementById('result').innerHTML = message;
}

function setPrefs (list, prefs) {
	setOrder(list, prefs.order);
	var max = list.slice(0, prefs.max_tabs);
	return max;
}

function setOrder (list, order) {

	if (order == "old") {
		list.reverse();
	}
	//default ordered

	return list;
}

function openSeps(html, prefs) {
	var links = getLinks(html);
	links = setPrefs(links, prefs);
	openTabs(links, prefs.focus);
	return links;
}

function openLinks (html, prefs) {
	// console.log("--------- begin ---------");
	var links = openSeps(html, prefs);
	panelWrite("Separadores por ler: " + links.length);
	// console.log("---------  end  ---------");
}

function getLinks (html) {

	var container = document.createElement("p");
	container.innerHTML = html;

	var subject = container.getElementsByClassName("subject");
	var lastpost = container.getElementsByClassName("lastpost");

	var list = [];
	for (var i = 0; i < subject.length; i++) {

		links = subject[i].getElementsByTagName("a");

		var sub = links[0].text;
		var url = links[1].href;
		var by = lastpost[i].lastElementChild.text;

		var elem = {
			"subject": sub,
			"by": by,
			"url": url
		};

		list.push(elem); //reverse
	}

	return list;
}

function openTab (url, focus) {
	if (focus) {
		self.port.emit("openTab", url);
	} else {
		self.port.emit("openTab", {
			url: url,
			inBackground: true
		});	
	}
}

function openTabs (urls, focus) {

	if (urls.length) {
		openTab(urls[0].url, focus);
	}
	for (var i = 1; i < urls.length; i++) {	
		openTab(urls[i].url);
	}
}


// Verifica se 'str' aparece em 'text'
function contains (text, str) {
	return text.indexOf(str) >= 0;
}
