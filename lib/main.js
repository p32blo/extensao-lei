
var { ToggleButton } = require("sdk/ui/button/toggle");
var Panel = require("sdk/panel").Panel;

var Request = require("sdk/request").Request;
var notifications = require("sdk/notifications");
var timers = require("sdk/timers");

var tabs = require("sdk/tabs");
var data = require("sdk/self").data;

// Getting user preferences
var preferences = require("sdk/simple-prefs")
var prefs = preferences.prefs;

var max = 0;
var last = 0;


// Verifica se 'str' aparece em 'text'
function contains (text, str) {
	return text.indexOf(str) >= 0;
}


// usar as preferencias para formatar o url
function get_url () {
	return prefs.forum_url + "?action=" + prefs.mode;
}
	 
// Called on Request completition
function actionPanel (response) {
	//html response
	var data = response.text;

	// No response
	if (!contains(response.statusText, "OK")) {
		panel.resize(350, 40);
		panel.port.emit("panel", "Ocorreu um erro. O servidor pode estar offline.");
	} else if (contains(data, ">Login<")) {
		// console.log("LOGGED OUT");
		panel.hide();
		tabs.open(get_url());
	} else {
		// console.log("LOGGED IN");
		panel.port.emit("data", data, prefs);
	}
}

function request (url, callback) {
	// request for the url
	Request({ 
		url: url,
		onComplete: callback
	}).get();
}


var button = ToggleButton({
	id: "forum-tabs-open",
	label: "Abrir Separadores",
	icon: {
		"16": "./icon-16.png",
		"32": "./icon-32.png",
		"64": "./icon-64.png"
	},
	onChange: handleShow
});

var panel = Panel({
	contentURL: data.url("panel.html"),
	contentScriptFile: [data.url("panel.js")],
	contentScriptWhen: "ready",
	onHide: handleHide
});

function handleShow (state) {
	if (state.checked) {

		panel.show({
			width: 200,
			height: 40,
			position: button
		});

		//console.log("LOADED");
		panel.port.emit("Loaded");

		panel.port.emit("panel", "A esperar pelo Servidor...");
		request(get_url(), actionPanel);
	}
}

function handleHide () {
	button.state('window', {checked: false});

	//console.log("UN-LOADED");
	panel.port.emit("Unloaded");
}


panel.port.on("openTab", tabs.open);

panel.port.on("notify", function (data_list) {
	console.log("NOTIFY IN");

	if (last > max) {

		max = last;

		var str = "";
		for (var i = 0; i < data_list.length; i++) {
			str += data_list[i].subject + " | " + data_list[i].by;
			str += "\n";
		};

		console.log("--- NOTIFICATION ---");
		// console.log("\n" + str);

		notifications.notify({
			title: "Fórum Lei",
			iconURL: data.url("icon.png"),
			text: str,
			onClick: function () {
				console.log("CLICK");
				panel.port.emit("clicked", data_list, prefs)
			}
		});
	}
	console.log("NOTIFY OUT");
});

function checkNewMessage () {
	request(get_url(), function (response) {

		var html = response.text;

		if (contains(response.statusText, "OK") && 
			!contains(html, ">Login<")) {

			var res = html.match(/#msg([0-9]+)/);

			if (res) {
				last = res[1];
				console.log(last + "/" + max);
				panel.port.emit("unread", html);
			}
		}
	});
}

function setTimer (notify, miliseconds, id) {
	var res = null;
	if (notify) {
		checkNewMessage();
		res = timers.setInterval(checkNewMessage, miliseconds);
	}
	else if (id != null && id != undefined) {
		timers.clearInterval(id);
	}
	return res;
	
}

var timerID = setTimer(prefs.notify, prefs.notify_freq * 1000);

preferences.on("notify", function () {
	timerID = setTimer(prefs.notify, prefs.notify_freq * 1000, timerID);
});

preferences.on("notify_freq", function () {
	if (prefs.notify_freq < 10) 
		prefs.notify_freq = 10;
	timerID = setTimer(prefs.notify, prefs.notify_freq * 1000, timerID);
});

preferences.on("max_tabs", function () {
	if (prefs.max_tabs > 50) 
		prefs.max_tabs = 50;
});